import 'package:flutter/material.dart';

class ColorCollections {
  /// COMMON COLORS
  static const Color backgroundColor1 = Color(0xFFF5F6F8);
  static const Color backgroundColor2 = Color(0xFFFFFFFF);
  static const Color primaryColor = Color(0xff43AF9F);
  static const Color errorColor = Color(0xffC40C0C);
  static const Color successColor = Color(0xff41B06E);

  /// TEXT COLORS
  static const Color primaryTextColor = Color(0xff4D504F);
  static const Color secondaryTextColor = Color(0xffA09A9A);
}
