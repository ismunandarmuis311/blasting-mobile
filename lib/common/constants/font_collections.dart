import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:blasting_app/common/constants/color_collections.dart';

class FontCollections {
  /// FONT STYLES
  static TextStyle primaryTextStyle = GoogleFonts.nunitoSans(
    color: ColorCollections.primaryTextColor,
  );
  static TextStyle secondaryTextStyle = GoogleFonts.nunitoSans(
    color: ColorCollections.secondaryTextColor,
  );

  /// FONT WEIGHT
  static const FontWeight light = FontWeight.w300;
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight medium = FontWeight.w500;
  static const FontWeight semiBold = FontWeight.w600;
  static const FontWeight bold = FontWeight.w700;
  static const FontWeight extraBold = FontWeight.w800;
}
