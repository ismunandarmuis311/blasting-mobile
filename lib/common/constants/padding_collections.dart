class PaddingCollections {
  static const double defaultPadding = 18.0;
  static const double defaultPaddingLarge = 24.0;
  static const double defaultPaddingSmall = 12.0;
  static const double defaultPaddingMedium = 16.0;
}
